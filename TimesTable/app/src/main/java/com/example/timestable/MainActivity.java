package com.example.timestable;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    final int maxValue = 20;
    final int minValue = 1;
    final int stepValue = 1;

    ListView myListView;
    SeekBar mySeekBar;
    ArrayList<Integer> tableValues;
    ArrayAdapter<Integer> arrayAdapter;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myListView = (ListView) findViewById(R.id.listView);
        mySeekBar = (SeekBar) findViewById(R.id.seekBar);

        mySeekBar.setMin(minValue);
        mySeekBar.setMax(maxValue);
        mySeekBar.setProgress(stepValue);

        tableValues = new ArrayList<Integer>();
        for (int i = 0; i < 20; i++) {
            tableValues.add(i + 1);
        }

        arrayAdapter = new ArrayAdapter<Integer>(this, android.R.layout.simple_list_item_1, tableValues);
        myListView.setAdapter(arrayAdapter);

        mySeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                //Toast.makeText(MainActivity.this, "Seek bar progress is: " + progress,  Toast.LENGTH_SHORT).show();

                for (int i = 0; i < 20; i++) {
                    tableValues.set(i, (i + 1) * progress);
                }
                arrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }
}
