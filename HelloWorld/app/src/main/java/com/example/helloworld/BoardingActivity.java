package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.text.HtmlCompat;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class BoardingActivity extends AppCompatActivity {

    ViewPager viewPager;
    LinearLayout dotsLayout;
    SliderAdapter sliderAdapter;
    TextView[] dots;
    Button getStartedButton;
    Button nextButton;
    int pagerStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide(); //Remove title bar
        setContentView(R.layout.activity_boarding);

        Log.d("Debug", "STARTING BOARDING ACTIVITY");

        //Hide get started button at first instance
        getStartedButton = (Button) findViewById(R.id.get_started_button);
        getStartedButton.setVisibility(View.GONE);

        //Hooks
        viewPager = findViewById(R.id.slider);
        dotsLayout = findViewById(R.id.dots);

        //Call adapter
        sliderAdapter = new SliderAdapter(this);
        viewPager.setAdapter(sliderAdapter);

        //Functionality to next button
        pagerStatus = 0;
        nextButton = findViewById(R.id.next_btn);
        nextButton.setOnClickListener(nextButtonListener);

        addDots(0);
        viewPager.addOnPageChangeListener(changeListener);
    }

    // Create an anonymous implementation of OnClickListener
    private View.OnClickListener nextButtonListener = new View.OnClickListener() {
        public void onClick(View v) {
            changeListener.onPageSelected(pagerStatus);
        }
    };

    public void onClickGetStarted(View view) {
        Intent intent = new Intent(BoardingActivity.this, VideoActivity.class);
        startActivity(intent);
        finish();
    }

    private void addDots(int position) {
        dots = new TextView[sliderAdapter.getCount()];
        dotsLayout.removeAllViews();

        for(int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(HtmlCompat.fromHtml("&#8226", HtmlCompat.FROM_HTML_MODE_LEGACY));
            dots[i].setTextSize(35);

            dotsLayout.addView(dots[i]);
        }

        if(dots.length > 0) {
            dots[position].setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    ViewPager.OnPageChangeListener changeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            if(position == sliderAdapter.getCount() - 1) {
                getStartedButton.setVisibility(View.VISIBLE);
                nextButton.setText("<");
            }
            else {
                getStartedButton.setVisibility(View.GONE);
                nextButton.setText(">");
            }

            addDots(position);
            pagerStatus = position;
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };
}