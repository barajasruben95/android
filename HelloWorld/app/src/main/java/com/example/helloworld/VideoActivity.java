package com.example.helloworld;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.MediaController;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;

public class VideoActivity extends AppCompatActivity {

    VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide(); //Remove title bar
        setContentView(R.layout.activity_video);

        Log.d("Debug", "STARTING VIDEO ACTIVITY");

        //Create video object
        videoView = (VideoView) findViewById(R.id.be_videoView);
        String videoString = "android.resource://" + getPackageName() + "/" + R.raw.intro;
        Uri videoUri = Uri.parse(videoString);
        videoView.setVideoURI(videoUri);

        //Create media object
        MediaController mediaController = new MediaController(this);
        videoView.setMediaController(mediaController);
        mediaController.setAnchorView(videoView);
        videoView.start();
    }

    public void onClickGetStarted(View view) {
        Intent intent = new Intent(VideoActivity.this, VideoActivity02.class);
        startActivity(intent);
        finish();
    }
}