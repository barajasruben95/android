package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.MediaController;
import android.widget.VideoView;

public class VideoActivity08 extends AppCompatActivity {

    VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide(); //Remove title bar
        setContentView(R.layout.activity_video08);
        Log.d("Debug", "STARTING VIDEO08 ACTIVITY");

        //Create video object
        videoView = (VideoView) findViewById(R.id.be_videoView08);
        String videoString = "android.resource://" + getPackageName() + "/" + R.raw.mi_app;
        Uri videoUri = Uri.parse(videoString);
        videoView.setVideoURI(videoUri);

        //Create media object
        MediaController mediaController = new MediaController(this);
        videoView.setMediaController(mediaController);
        mediaController.setAnchorView(videoView);
        videoView.start();
    }

    public void onClickPrevious(View view) {
        Intent intent = new Intent(VideoActivity08.this, VideoActivity07.class);
        startActivity(intent);
        finish();
    }

    public void onClickNext(View view) {
        Intent intent = new Intent(VideoActivity08.this, VideoActivity09.class);
        startActivity(intent);
        finish();
    }
}