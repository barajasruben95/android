package com.example.helloworld;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager.widget.PagerAdapter;

public class SliderAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;
    int images[] = {
            R.drawable.slider01,
            R.drawable.slider02,
            R.drawable.slider03,
            R.drawable.slider04,
            R.drawable.slider05,
            R.drawable.slider06,
    };

    int headings[] = {
            R.string.boarding_description01,
            R.string.boarding_description02,
            R.string.boarding_description03,
            R.string.boarding_description04,
            R.string.boarding_description05,
            R.string.boarding_description06
    };

    int descriptions[] = {
            R.string.boarding_description01,
            R.string.boarding_description02,
            R.string.boarding_description03,
            R.string.boarding_description04,
            R.string.boarding_description05,
            R.string.boarding_description06
    };

    public SliderAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return headings.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == (ConstraintLayout) object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slider_layout, container, false);

        //Hooks
        ImageView imageView = view.findViewById(R.id.slider_image);
        TextView heading = view.findViewById(R.id.slider_heading);
        TextView description = view.findViewById(R.id.slider_description);

        imageView.setImageResource(images[position]);
        heading.setText(headings[position]);
        description.setText(descriptions[position]);

        container.addView(view);

        Log.d("Debug", "POSITION: " + Integer.toString(position));

        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((ConstraintLayout) object);
    }
}
