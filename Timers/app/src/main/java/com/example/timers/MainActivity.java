package com.example.timers;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*//Way 1
        final Handler handler = new Handler();
        Runnable runnable =new Runnable() {
            @Override
            public void run() {
                Log.d("Log", "A second has elapsed");
                handler.postDelayed(this, 5000);
            }
        };

        handler.post(runnable);*/

        //Way 2
        new CountDownTimer(10000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                Log.d("Debug", String.valueOf(millisUntilFinished/1000));
            }

            @Override
            public void onFinish() {
                Log.d("Debug", "Timer has finished");
            }
        }.start();
    }
}
